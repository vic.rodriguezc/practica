package cl.bennu.capabilities.service;

import cl.bennu.capabilities.common.domain.*;
import cl.bennu.capabilities.mapper.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Date;


public class ServicioTest {

    Servicio servicio;

    RolMapper rolMapper;
    PersonaMapper personaMapper;
    DepartamentoMapper departamentoMapper;
    EmpresaMapper empresaMapper;
    ServiceMapper serviceMapper;

    WorkSpaceMapper workSpaceMapper;

    @BeforeEach
    public void init() {

        List<Persona> listPersona = new ArrayList<>();
        Persona persona = new Persona();
        persona.setApellidoMaterno("rodriguez");
        persona.setApellidoPaterno("coronado");
        persona.setDireccion("Max jara 4365");
        persona.setEmail("victor.rodriguez@gmail.com");
        persona.setRut(240507420);
        persona.setTelefono(9684365);
        persona.setNombres("victor manoel");
        persona.setId(null);
        persona.setId(1234L);

        Persona persona2 = new Persona();
        persona2.setApellidoMaterno("rodriguez");
        persona2.setApellidoPaterno("coronado");
        persona2.setDireccion("Max jara 4365");
        persona2.setEmail("victor.rodriguez@gmail.com");
        persona2.setRut(240507422);
        persona2.setTelefono(9684361);
        persona2.setNombres("Joel manoel");
        persona2.setId(null);
        persona2.setId(1233L);

        listPersona.add(persona);
        listPersona.add(persona2);

        List<Service> listServicio = new ArrayList<>();
        Service newServicio = new Service();
        Service newServicio2 = new Service();
        listServicio.add(newServicio);
        listServicio.add(newServicio2);

        serviceMapper = Mockito.mock(ServiceMapper.class);
        Mockito.when(serviceMapper.getAll()).thenReturn(listServicio);
        Mockito.when(serviceMapper.getById(2)).thenReturn(newServicio);
        personaMapper = Mockito.mock(PersonaMapper.class);
        Mockito.when(personaMapper.getAllPersona()).thenReturn(listPersona);
        Mockito.when(personaMapper.getPersonaById(1234l)).thenReturn(persona);
        Mockito.when(personaMapper.findPersona("victor")).thenReturn(listPersona);
        empresaMapper = Mockito.mock(EmpresaMapper.class);
        Mockito.when(empresaMapper.getAll()).thenReturn(null);
        Mockito.when(empresaMapper.getById(2l)).thenReturn(null);

        servicio = new Servicio(rolMapper, personaMapper, departamentoMapper, empresaMapper, serviceMapper, workSpaceMapper);
        List<Rol> listaRoles = new ArrayList<>();

        Rol rol1 = new Rol();
        rol1.setId(1L);
        rol1.setCodigo("codigo1");
        rol1.setNombre("nombre1");
        rol1.setDescripcion("descripcion1");

        Rol rol2 = new Rol();
        rol2.setId(2L);
        rol2.setCodigo("codigo2");
        rol2.setNombre("nombre2");
        rol2.setDescripcion("descripcion2");

        listaRoles.add(rol1);
        listaRoles.add(rol2);

        rolMapper = Mockito.mock(RolMapper.class);
        servicio = new Servicio(rolMapper, personaMapper, departamentoMapper, empresaMapper, serviceMapper, workSpaceMapper);
        Mockito.when(rolMapper.getAll()).thenReturn(listaRoles);
        Mockito.when(rolMapper.getById(1L)).thenReturn(rol1);
        Mockito.when(rolMapper.getById(2L)).thenReturn(rol2);
        departamentoMapper = Mockito.mock(DepartamentoMapper.class);

        Mockito.when(departamentoMapper.getAll()).thenReturn(null);

        Mockito.when(departamentoMapper.getById(10L)).thenReturn(null);

        servicio = new Servicio(rolMapper, personaMapper, departamentoMapper, empresaMapper, serviceMapper, workSpaceMapper);
    }



    @Test
    public void shouldGetAllRolWorks() throws Exception {
        List<Rol> result = servicio.getAllRol();
        System.out.printf("[getAll] Salida: " + result + "\n");
    }

    @Test
    public void shouldGetByIdServiceWorks() {
        servicio.getServiceById(2L);
    }

    @Test
    public void shouldGetByIdWorks() {
        Rol rol = servicio.getRolById(2L);
        System.out.printf("[getById] Salida: " + rol + "\n");
    }

    @Test
    public void shouldInsertServiceWorks() throws Exception {

        Service objServicio = new Service();
        objServicio.setId(1L);
        objServicio.setWorkspaceId(1L);
        objServicio.setNombre("test");
        objServicio.setDescripcion("SET");
        objServicio.setEndpoint("/test");
        objServicio.setOpenapi("test");
        objServicio.setActivo(true);
        objServicio.setFechaCreacion(new Date());
        objServicio.setFechaActualizacion(new Date());

        serviceMapper.insert(objServicio);
    }

    @Test
    public void shouldUpdateServiceWorks() {
        Service service = new Service();
        servicio.saveService(service);
    }

    @Test
    public void shouldDeleteServiceWorks() {
        serviceMapper.deleteById(1L);
    }
    public void shouldSaveRolWorks() {
        Rol rol = new Rol();
        rol.setId(3L);
        rol.setCodigo("codigo3");
        rol.setNombre("nombre3");
        rol.setDescripcion("descripcion3");

        servicio.saveRol(rol);
    }

    @Test
    public void ShouldDeleteRolWorks() {
        servicio.deleteRol(1L);
    }

    @Test
    public void shouldGetAllPersona() {

        List<Persona> result= servicio.getAllPersona();
        System.out.println(result);
    }
    @Test
    public void shouldGetByIdPersona(){

        Persona result = servicio.getById(1234L);
        System.out.print(result.getNombres());
    }
    @Test
    public void shouldFindPersona(){

        servicio.findPersona("victor");
    }

    @Test
    public void shouldInsertPersona(){
        Persona persona = new Persona();
        persona.setApellidoMaterno("rodriguez");
        persona.setApellidoPaterno("coronado");
        persona.setDireccion("Max jara 4365");
        persona.setEmail("victor.rodriguez@gmail.com");
        persona.setRut(240507420);
        persona.setTelefono(9684365);
        persona.setNombres("victor manoel");
        persona.setId(null);
        servicio.save(persona);
    }
    @Test
    public void shouldDeletePersona(){
        servicio.deletePersona(1234L);
    }
    @Test
    public void  shouldUpdatePersona(){
        Persona persona = new Persona();
        persona.setApellidoMaterno("rodriguez");
        persona.setApellidoPaterno("coronado");
        persona.setDireccion("Max jara 4365");
        persona.setEmail("victor.rodriguez@gmail.com");
        persona.setRut(240507420);
        persona.setTelefono(9684365);
        persona.setNombres("victor manoel");
        persona.setId(null);
        servicio.putPersona(persona);
    }
    @Test
    public void shouldGetAllgetAllEmpresasWorks() {
        servicio.getAllEmpresas();
    }

    @Test
    public void shouldGetEmpresaByIdWorks() {
        servicio.getEmpresaById(2l);
    }

    @Test
    public void shouldSaveEmpresa() {
        Empresa empresa = new Empresa();
        servicio.saveEmpresa(empresa);
    }

    @Test
    public void shouldDeleteEmpresa() {
        servicio.deleteEmpresa(2l);
    }

    @Test
    public void shouldGetAllServiceWorks() {

        List<Service> result = servicio.getAllServices();
    }

    @Test
    public void shouldGetAllDepartamento() {
        servicio.getAllDepartamento();
    }

    @Test
    public void shouldInsertDepartamento() {
        Departamento departamento = new Departamento(1L, 1L, "nombreX", "empresaX", "responsableNegocioX", "respondableItX", true, new Date(12 - 05 - 2022), new Date(12 - 05 - 202));
        servicio.saveDepartamento(departamento);
    }


    @Test
    public void shouldUpdateDepartamento() {
        Departamento departamento = new Departamento(2L, 13L, "nombreY", "empresaY", "responsableNegocioY", "respondableItY", true, new Date(12 - 05 - 2022), new Date(12 - 05 - 2022));
        servicio.saveDepartamento(departamento);
    }



    @Test
    public void shouldDeleteDepartamento() {
        servicio.deleteDepartamento(10L);
    }

    @Test
    public void shouldGetByIdDepartamento() {
        Departamento departamentoById;
        departamentoById = servicio.getDepartamentoById(10L);
    }


}