create table persona (
    id serial unique not null

    , rut integer unique not null
    , dv varchar(1) not null
    , nombres varchar(200) not null
    , appellido_paterno varchar(100) not null
    , appellido_materno varchar(100) null
    , direccion varchar(500) null
    , email varchar(200) null
    , telefono integer null

    , primary key (id)
);

create table rol (
    id serial unique not null

    , codigo varchar(10) unique not null
    , nombre varchar(40) not null
    , descripcion varchar(500) null

    , primary key (id)
);

create table usuario (
    id serial unique not null
    , persona_id integer not null

    , usuario varchar(10) unique not null
    , contrasenna varchar(40) not null
    , activo boolean not null
    , fecha_creacion timestamp not null
    , fecha_actualizacion timestamp null

    , primary key (id)
    , foreign key (persona_id) references persona(id)
);

create table usuario_rol (
    rol_id integer not null
    , usuario_id integer not null

    , foreign key (usuario_id) references usuario(id)
    , foreign key (rol_id) references rol(id)
);

create table empresa (
    id serial unique not null
    , usuario_id integer not null

    , nombre varchar(10) unique not null
    , inicio_actividades date null
    , direccion varchar(40) null
    , descripcion varchar(500) null
    , contacto varchar(500) null
    , telefono integer null

    , activo boolean not null
    , fecha_creacion timestamp not null
    , fecha_actualizacion timestamp null

    , primary key (id)
    , foreign key (usuario_id) references usuario(id)
);

create table departamento (
    id serial unique not null
    , empresa_id integer not null

    , nombre varchar(10) unique not null
    , descripcion varchar(500) null
    , responsable_negocio varchar(500) null
    , responsable_it varchar(500) null

    , activo boolean not null
    , fecha_creacion timestamp not null
    , fecha_actualizacion timestamp null

    , primary key (id)
    , foreign key (empresa_id) references empresa(id)
);

create table workspace (
    id serial unique not null
    , departamento_id integer not null

    , nombre varchar(10) unique not null
    , descripcion varchar(500) null

    , activo boolean not null
    , fecha_creacion timestamp not null
    , fecha_actualizacion timestamp null

    , primary key (id)
    , foreign key (departamento_id) references departamento(id)
);

create table servicio (
    id serial unique not null
    , workspace_id integer not null

    , nombre varchar(10) unique not null
    , descripcion varchar(500) null
    , endpoint varchar(500) not null
    , openapi text null

    , activo boolean not null
    , fecha_creacion timestamp not null
    , fecha_actualizacion timestamp null

    , primary key (id)
    , foreign key (workspace_id) references workspace(id)
);
