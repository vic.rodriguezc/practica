package cl.bennu.capabilities.mapper;

import cl.bennu.capabilities.common.domain.Service;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface ServiceMapper {

    List<Service> getAll();

    Service getById(long id);

    void insert(Service service);

    void update(Service service);

    void deleteById(Long id);

}
