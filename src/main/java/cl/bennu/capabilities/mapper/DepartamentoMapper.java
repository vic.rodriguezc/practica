package cl.bennu.capabilities.mapper;

import cl.bennu.capabilities.common.domain.Departamento;
import cl.bennu.capabilities.common.domain.query.DepartamentoQuery;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface DepartamentoMapper {
    List<Departamento> getAll();

    List<Departamento> find(DepartamentoQuery query);

    Departamento getById(Long id);

    void insert(Departamento departamento);

    void update(Departamento departamento);

    void delete(Long id);

}
