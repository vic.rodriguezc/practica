package cl.bennu.capabilities.mapper;

import cl.bennu.capabilities.common.domain.WorkSpace;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface WorkSpaceMapper {

    List<WorkSpace> getAll();

    WorkSpace getById(Long id);

    void insert(WorkSpace workSpace);

    void update(WorkSpace workSpace);

    void delete(Long id);

}
