package cl.bennu.capabilities.mapper;


import cl.bennu.capabilities.common.domain.Rol;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface RolMapper {

    List<Rol> getAll();

    Rol getById(Long id);

    void insert(Rol rol);

    void update(Rol rol);

    void delete(Long id);

    //Rol deleteByCode(String codigo);


    // List<Rol> find(RolQuery query);


}
