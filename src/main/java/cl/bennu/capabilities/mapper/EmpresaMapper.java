package cl.bennu.capabilities.mapper;

import cl.bennu.capabilities.common.domain.Empresa;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface EmpresaMapper {

    List<Empresa> getAll();

    Empresa getById(Long id);

    void insert(Empresa empresa);

    void update(Empresa empresa);

    void delete(Long id);

}
