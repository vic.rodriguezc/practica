package cl.bennu.capabilities.mapper;

import cl.bennu.capabilities.common.domain.Persona;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface PersonaMapper {

    List<Persona> getAllPersona();

    Persona getPersonaById(Long id);

    void insert(Persona persona);

    void delete(Long id);

    void update(Persona persona);

    List<Persona> findPersona(String nombre);

}
