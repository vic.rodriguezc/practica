package cl.bennu.capabilities.common.domain.query;

public class PersonaQuery {

    private String nombres;

    public String getNombre() {
        return nombres;
    }

    public void setNombre(String nombres) {
        this.nombres = nombres;
    }
}
