package cl.bennu.capabilities.common.domain;

import java.util.Date;

public class Departamento {

    private Long id;
    private Long empresaId;
    private String nombre;
    private String descripcion;
    private String responsableNegocio;
    private String responsableIt;
    private Boolean activo;
    private Date fechaCreacion;
    private Date fechaActualizacion;

    public Departamento(Long id, Long empresaId, String nombre, String descripcion, String responsableNegocio, String responsableIt, Boolean activo, Date fechaCreacion, Date fechaActualizacion) {
        this.id = id;
        this.empresaId = empresaId;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.responsableNegocio = responsableNegocio;
        this.responsableIt = responsableIt;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
        this.fechaActualizacion = fechaActualizacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Long empresaId) {
        this.empresaId = empresaId;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getResponsableNegocio() {
        return responsableNegocio;
    }

    public void setResponsableNegocio(String responsableNegocio) {
        this.responsableNegocio = responsableNegocio;
    }

    public String getResponsableIt() {
        return responsableIt;
    }

    public void setResponsableIt(String responsableIt) {
        this.responsableIt = responsableIt;
    }


    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }
}
