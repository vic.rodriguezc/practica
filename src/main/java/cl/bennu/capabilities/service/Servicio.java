package cl.bennu.capabilities.service;

import cl.bennu.capabilities.common.domain.*;
import cl.bennu.capabilities.common.domain.query.DepartamentoQuery;
import cl.bennu.capabilities.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class Servicio {


    private WorkSpaceMapper workSpaceMapper;
    private RolMapper rolMapper;

    private PersonaMapper personaMapper;

    private DepartamentoMapper departamentoMapper;

    private EmpresaMapper empresaMapper;

    private ServiceMapper serviceMapper;

    public Servicio(RolMapper rolMapper, PersonaMapper personaMapper, DepartamentoMapper departamentoMapper, EmpresaMapper empresaMapper, ServiceMapper serviceMapper, WorkSpaceMapper workSpaceMapper) {
        this.rolMapper = rolMapper;
        this.personaMapper = personaMapper;
        this.departamentoMapper = departamentoMapper;
        this.empresaMapper = empresaMapper;
        this.serviceMapper = serviceMapper;
        this.workSpaceMapper = workSpaceMapper;
    }


    public List<Rol> getAllRol() {
        return rolMapper.getAll();
    }

    public Rol getRolById(Long id) {
        return rolMapper.getById(id);
    }

    public void deleteRol(Long id) {
        rolMapper.delete(id);
    }

    public void saveRol(Rol rol) {
        if (rol.getId() == null) {
            rolMapper.insert(rol);
        } else {
            rolMapper.update(rol);
        }
    }

    public List<Departamento> getAllDepartamento() {
        return departamentoMapper.getAll();
    }

    public List<Departamento> findDepartamento(DepartamentoQuery query) {
        return departamentoMapper.find(query);
    }

    public Departamento getDepartamentoById(Long id) {
        return departamentoMapper.getById(id);
    }

    public void saveDepartamento(Departamento departamento) {
        if (departamento.getId() == null) {
            departamentoMapper.insert(departamento);
        } else {
            departamentoMapper.update(departamento);
        }
    }

    public void deleteDepartamento(Long id) {
        departamentoMapper.delete(id);

    }

    public List<Persona> getAllPersona() {
        return personaMapper.getAllPersona();
    }

    public Persona getById(Long id) {
        return personaMapper.getPersonaById(id);
    }

    public void save(Persona persona) {
        System.out.println("PRUEBA: " + persona.getId());
        if (persona.getId() == null) {
            personaMapper.insert(persona);
        } else {
            personaMapper.update(persona);
        }
    }

    public void deletePersona(Long id) {
        personaMapper.delete(id);
    }

    public void putPersona(Persona persona) {
        personaMapper.update(persona);
    }

    public List<Persona> findPersona(String nombre) {
        return personaMapper.findPersona(nombre);
    }

    public List<Empresa> getAllEmpresas() {
        return empresaMapper.getAll();
    }

    public Empresa getEmpresaById(Long id) {
        return empresaMapper.getById(id);
    }

    public void saveEmpresa(Empresa empresa) {
        if (empresa.getId() == null) {
            empresaMapper.insert(empresa);
        } else {
            empresaMapper.update(empresa);
        }
    }

    public void deleteEmpresa(Long id) {
        empresaMapper.delete(id);
    }

    public List<cl.bennu.capabilities.common.domain.Service> getAllServices() {
        return serviceMapper.getAll();
    }

    public cl.bennu.capabilities.common.domain.Service getServiceById(Long id) {
        return serviceMapper.getById(id);
    }

    public void deleteServiceById(Long id) {
        serviceMapper.deleteById(id);


    }

    public cl.bennu.capabilities.common.domain.Service saveService(cl.bennu.capabilities.common.domain.Service service) {
        if (service.getId() == null) {
            serviceMapper.insert(service);
        } else {
            serviceMapper.update(service);
        }
        return service;
    }

    public List<WorkSpace> getAllWorkSpace() {
        return workSpaceMapper.getAll();
    }

    public WorkSpace getWorkSpaceById(Long id) {
        return workSpaceMapper.getById(id);
    }

    public void saveWorkSpace(WorkSpace workSpace) {
        if (workSpace.getId() == null) {
            workSpaceMapper.insert(workSpace);
        } else {
            workSpaceMapper.update(workSpace);
        }
    }

    public void deleteWorkSpace(Long id) {
        workSpaceMapper.delete(id);
    }


    public Integer suma(Integer x, Integer x2) {
        return x + x2;
    }


}
