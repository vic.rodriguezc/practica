package cl.bennu.capabilities.controller;

import cl.bennu.capabilities.common.domain.Departamento;
import cl.bennu.capabilities.common.domain.query.DepartamentoQuery;
import cl.bennu.capabilities.service.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DepartamentoController {

    @Autowired
    private Servicio servicio;

    @GetMapping("/departamento")
    public List<Departamento> get() {
        return servicio.getAllDepartamento();
    }


    @GetMapping("/departamento/{id}")
    public Departamento getById(@PathVariable Long id) {
        return servicio.getDepartamentoById(id);
    }

    @PatchMapping("/departamento")
    public List<Departamento> find(@RequestBody DepartamentoQuery query) {
        return servicio.findDepartamento(query);
    }

    @PostMapping("/departamento")
    public void save(@RequestBody Departamento departamento) {
        servicio.saveDepartamento(departamento);
    }

    @DeleteMapping("/departamento/{id}")
    public void delete(@PathVariable Long id) {
        servicio.deleteDepartamento(id);
    }

    @PutMapping("/departamento")
    public void update(@RequestBody Departamento departamento) {
        servicio.saveDepartamento(departamento);
    }


}
