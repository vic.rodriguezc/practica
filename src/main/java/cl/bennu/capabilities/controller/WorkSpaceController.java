package cl.bennu.capabilities.controller;

import cl.bennu.capabilities.common.domain.WorkSpace;
import cl.bennu.capabilities.service.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class WorkSpaceController {

    @Autowired
    private Servicio servicio;

    @GetMapping("/workspace")
    public List<WorkSpace> get() {
        return servicio.getAllWorkSpace();
    }

    @GetMapping("/workspace/{id}")
    public WorkSpace getById(@PathVariable Long id) {
        return servicio.getWorkSpaceById(id);
    }

    @PostMapping("/workspace")
    public void insert(@RequestBody WorkSpace workSpace) {
        servicio.saveWorkSpace(workSpace);
    }

    @PutMapping("/workspace")
    public void update(@RequestBody WorkSpace workSpace) {
        servicio.saveWorkSpace(workSpace);
    }

    @DeleteMapping("/workspace/{id}")
    public void delete(@PathVariable Long id) {
        servicio.deleteWorkSpace(id);
    }

}
