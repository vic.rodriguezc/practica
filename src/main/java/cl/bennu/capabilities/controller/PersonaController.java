package cl.bennu.capabilities.controller;


import cl.bennu.capabilities.common.domain.Persona;
import cl.bennu.capabilities.service.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonaController {

    @Autowired
    private Servicio servicio;

    @GetMapping("/persona")
    public List<Persona> get() {
        return servicio.getAllPersona();
    }

    @GetMapping("/persona/{id}")
    public Persona getById(@PathVariable Long id) {
        return servicio.getById(id);
    }

    @PostMapping("/persona")
    public void post(@RequestBody Persona persona) {
        servicio.save(persona);
    }

    @DeleteMapping("/persona/{id}")
    public void delete(@PathVariable Long id) {
        servicio.deletePersona(id);
    }

    @PutMapping("/persona")
    public void put(@RequestBody Persona persona) {
        servicio.putPersona(persona);
    }

    @PostMapping("/persona/{nombre}")
    public List<Persona> findPersona(@PathVariable String nombre) {
        return servicio.findPersona(nombre);
    }

}
