package cl.bennu.capabilities.controller;

import cl.bennu.capabilities.common.domain.Service;
import cl.bennu.capabilities.service.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ServiceController {

    @Autowired
    private Servicio servicio;


    @GetMapping("/servicio")
    public List<Service> get()  {
        return servicio.getAllServices();


    }

    @GetMapping("/servicio/{id}")
    public Service getbyid(@PathVariable long id) {
        return servicio.getServiceById(id);
    }


    @DeleteMapping("/servicio/{id}")
    public void deleteservice(@PathVariable Long id)  {
         servicio.deleteServiceById(id);
    }

    @PostMapping("/servicio")
    public void insertService(@RequestBody Service service) {
        servicio.saveService(service);

    }

    @PatchMapping("/servicio")
    public void updateService(@RequestBody Service service) {
        servicio.saveService(service);
    }

}
