package cl.bennu.capabilities.controller;

import cl.bennu.capabilities.common.domain.Rol;
import cl.bennu.capabilities.service.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RolController {

    @Autowired
    private Servicio servicio;

    @GetMapping("/rol")
    public List<Rol> get() throws Exception {
        return servicio.getAllRol();
    }

    @GetMapping("/rol/{id}")
    public Rol getById(@PathVariable Long id) throws Exception {
        return servicio.getRolById(id);
    }

    @PostMapping("/rol")
    public void insert(@RequestBody Rol rol) {

        servicio.saveRol(rol);
    }

    @PutMapping("/rol")
    public void update(@RequestBody Rol rol) {
        servicio.saveRol(rol);
    }

    @DeleteMapping("/rol/{id}")
    public void delete(@PathVariable Long id) {
        servicio.deleteRol(id);
    }

    /*@PatchMapping("/roles")
    public List<Rol> getById(@RequestBody RolQuery query) throws Exception {

        return servicio.findRoles(query);
    }
    */
}
