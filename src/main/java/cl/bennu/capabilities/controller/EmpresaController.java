package cl.bennu.capabilities.controller;

import cl.bennu.capabilities.common.domain.Empresa;
import cl.bennu.capabilities.service.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmpresaController {

    @Autowired
    private Servicio servicio;

    @GetMapping("/empresa")
    public List<Empresa> get() {
        return servicio.getAllEmpresas();
    }

    @GetMapping("/empresa/{id}")
    public Empresa getById(@PathVariable Long id) {
        return servicio.getEmpresaById(id);
    }

    @PostMapping("/empresa")
    public void insert(@RequestBody Empresa empresa) {
        servicio.saveEmpresa(empresa);
    }

    @PutMapping("/empresa")
    public void update(@RequestBody Empresa empresa) {
        servicio.saveEmpresa(empresa);
    }

    @DeleteMapping("/empresa/{id}")
    public void delete(@PathVariable Long id) {
        servicio.deleteEmpresa(id);
    }

}
